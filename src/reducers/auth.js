import {
    LOGOUT,
    LOGIN_STARTED,
    LOGIN_SUCCESS,
    LOGIN_FAILED
} from '../actions/action-types';

const initialState = {
    isLogin: sessionStorage.getItem('access_token') !== null || false,
    isLoading: false,
    error: ''
};

const auth = (state = initialState, { type, payload }) => {
    switch (type) {
        case LOGIN_STARTED: {
            return {
                ...state,
                isLoading: true
            };
        }
        case LOGIN_SUCCESS: {
            return {
                ...state,
                isLoading: false,
                isLogin: true
            };
        }
        case LOGIN_FAILED: {
            const { error } = payload;
            return {
                ...state,
                error,
                isLoading: false,
                isLogin: false
            };
        }
        case LOGOUT: {
            return {
                ...state,
                isLogin: false
            };
        }
        default: {
            return state;
        }
    }
};

export default auth;
