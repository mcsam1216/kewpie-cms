import { TOGGLE_MENU } from '../actions/action-types';

const initialState = {
    isMenuOpen: false
};

const uiSettings = (state = initialState, { type }) => {
    switch (type) {
        case TOGGLE_MENU: {
            return {
                ...state,
                isMenuOpen: !state.isMenuOpen
            };
        }
        default: {
            return state;
        }
    }
};

export default uiSettings;
