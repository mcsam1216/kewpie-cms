import { combineReducers } from 'redux';
import uiSettings from './ui';
import auth from './auth';

const rootReducer = combineReducers({ uiSettings, auth });

export default rootReducer;
