import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Header } from '../styles/layout';
import { logout } from '../actions/auth';
import { Button } from '../styles/common';
import { colors, devices, measurements } from '../constant';
import Menu from './menu';
import Icon from './icons';
import StaticIcon from './static-icon';

const Logo = styled.img`
    max-width: calc(100% - 60px);
    height: 100%;
    padding: 0 25px;

    @media ${devices.laptop} {
        height: 100%;
        width: auto;
    }
`;

const LogoutButton = styled(Button)`
    display: none;
    margin-left: auto;

    @media ${devices.laptop} {
        display: inherit;
    }
`;

const MobileMenu = styled.div`
    width: 100%;
    transform: ${({ isOpen }) => (isOpen ? 'scaleY(100%)' : 'scaleY(0)')};
    transform-origin: top;
    opacity: ${({ isOpen }) => (isOpen ? 1 : 0)};
    position: absolute;
    top: ${measurements.HEADER_HEIGHT};
    margin-left: -25px;
    box-shadow: 5px 1px 5px ${colors.HOVER};
    transition: opacity 0.5s, transform 0.5s ease-in-out;

    @media ${devices.laptop} {
        display: none;
    }
`;

const MenuIcon = styled(Icon)`
    display: block;
    cursor: pointer;

    #top, #middle, #bottom {
        transform: rotate(0deg);
        transition: transform .5s;
    }

    ${({ isopen }) =>
        isopen &&
        `
        #top {
            transform: translateX(35%) rotate(45deg);
            transition: transform .5s;
        }
        #middle {
            transform: scale(0);
            transition: transform .5s;
        }
        #bottom {
            transform: translateX(-35%) translateY(25%) rotate(-45deg);
            transition: transform .5s;
        }
    `}
    @media ${devices.laptop} {
        display: none;
    }
`;

const HeaderBar = ({ logout }) => {
    const [isOpen, openMenu] = useState(false);
    const [isLogout, setLogout] = useState(false);

    return (
        <Header>
            <MenuIcon
                icon="menu"
                white
                onClick={() => openMenu(!isOpen)}
                isopen={isOpen}
                width={30}
                height={30}
            />
            <Logo src="/assets/images/header-logo.png" />
            <LogoutButton
                color="secondary"
                onClick={() => {
                    setLogout(true);
                    logout();
                }}
            >
                <StaticIcon
                    icon="logout"
                    white
                    islogout={isLogout}
                    width={25}
                    height={25}
                />
                Log Out
            </LogoutButton>
            <MobileMenu isOpen={isOpen}>
                <Menu />
            </MobileMenu>
        </Header>
    );
};

HeaderBar.propTypes = {
    logout: PropTypes.func.isRequired
};

export default connect(null, { logout })(HeaderBar);
