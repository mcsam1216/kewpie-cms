import React from 'react';
import { Container, Wrapper, Title, Paragraph } from '../styles/common';

const Content = () => (
    <Container>
        <Wrapper>
            <Title>Title</Title>
            <Paragraph>This is a paragraph!</Paragraph>
        </Wrapper>
    </Container>
);

export default Content;
