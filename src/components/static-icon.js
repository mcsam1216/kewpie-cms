import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { colors } from '../constant';
import '../assets/icons/static/logout.svg';

const StyledSvg = styled.svg`
    --fill-color: ${({ primary, secondary, white }) => {
        if (primary) {
            return colors.PRIMARY;
        }
        if (secondary) {
            return colors.SECONDARY;
        }
        if (white) {
            return colors.WHITE;
        }
    }};
`;

const StaticIcon = ({ icon, ...props }) => (
    <StyledSvg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        {...props}
    >
        <use xlinkHref={`/assets/icons/static/sprite.svg#${icon}`} />
    </StyledSvg>
);

StaticIcon.defaultProps = {
    className: '',
    height: 30,
    width: 30,
    onClick: () => {},
    primary: false,
    secondary: false,
    white: false
};

StaticIcon.propTypes = {
    icon: PropTypes.string.isRequired,
    className: PropTypes.string,
    height: PropTypes.number,
    width: PropTypes.number,
    onClick: PropTypes.func,
    primary: PropTypes.bool,
    secondary: PropTypes.bool,
    white: PropTypes.bool
};

export default StaticIcon;
