import React from 'react';
import styled from 'styled-components';
import { menus, fonts, colors } from '../constant';

const MenuItem = styled.div`
    cursor: pointer;
    padding: 20px 25px;
    text-align: center;
    font-size: ${fonts.FONT_SIZE_M};

    &:hover {
        background: ${colors.HOVER};
    }
`;

const Menu = () => (
    <>
        {Object.keys(menus).map(key => {
            const menu = menus[key];
            return <MenuItem key={key}>{menu}</MenuItem>;
        })}
    </>
);

export default Menu;
