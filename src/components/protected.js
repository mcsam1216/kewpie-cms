import React from 'react';
import styled from 'styled-components';
import HeaderBar from './header-bar';
import { Body } from '../styles/layout';
import Menu from './menu';
import Content from './content';
import { colors, devices } from '../constant';

const SideMenu = styled.div`
    height: 100vh;
    width: 20%;
    max-width: 400px;
    display: flex;
    flex-direction: column;
    background: ${colors.WHITE};
    border-right: solid 1px ${colors.LIGHT};
    display: none;

    @media ${devices.laptop} {
        display: flex;
    }
`;

const Protected = () => (
    <>
        <HeaderBar />
        <Body>
            <SideMenu>
                <Menu />
            </SideMenu>
            <Content />
        </Body>
    </>
);

export default Protected;
