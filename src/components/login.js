import React from 'react';
import styled from 'styled-components';
import { useForm } from 'react-hook-form';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { login } from '../actions/auth';
import { colors } from '../constant';

const ErrorMessage = styled.div`
    color: ${colors.DANGER};
`;

const Login = ({ error, isLoading, login }) => {
    const { register, handleSubmit, errors } = useForm();

    return (
        <div>
            <h1>LOGIN PAGE</h1>
            {error && <ErrorMessage>{error}</ErrorMessage>}
            {isLoading && <div>Logging In...</div>}
            <form onSubmit={handleSubmit(login)}>
                <input name="username" ref={register({ required: true })} />
                {errors.username && (
                    <ErrorMessage>Username is required</ErrorMessage>
                )}
                <input
                    name="password"
                    type="password"
                    ref={register({ required: true })}
                />
                {errors.password && (
                    <ErrorMessage>Password is required</ErrorMessage>
                )}
                <input type="submit" />
            </form>
        </div>
    );
};

Login.propTypes = {
    error: PropTypes.string.isRequired,
    isLoading: PropTypes.bool.isRequired,
    login: PropTypes.func.isRequired
};

const mapStateToProps = ({ auth: { isLoading, error } }) => ({
    isLoading,
    error
});

export default connect(mapStateToProps, { login })(Login);
