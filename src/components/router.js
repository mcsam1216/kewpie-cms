import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router-dom';
import Login from './login';
import Protected from './protected';

const Router = ({ isLogin }) => (
    <>
        {isLogin && <Redirect to="/" />}
        {!isLogin && <Redirect to="/login" />}
        <Switch>
            <Route exact path="/login" component={Login} />
            <PrivateRoute path="/" component={Protected} isLogin={isLogin} />
        </Switch>
    </>
);

Router.propTypes = {
    isLogin: PropTypes.bool.isRequired
};

const PrivateRoute = ({ component: Component, isLogin, ...rest }) => (
    <Route
        {...rest}
        render={props =>
            isLogin ? (
                <Component {...props} />
            ) : (
                <Redirect to={{ pathname: '/login' }} />
            )
        }
    />
);

PrivateRoute.propTypes = {
    component: PropTypes.elementType.isRequired,
    isLogin: PropTypes.bool.isRequired
};

const mapStateToProps = state => {
    return { isLogin: state.auth.isLogin };
};

export default connect(mapStateToProps)(Router);
