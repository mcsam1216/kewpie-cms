import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { colors } from '../constant';
import MenuIcon from '../assets/icons/dynamic/menu.svg';

const ColorIcon = styled.div`
    --fill-color: ${({ primary, secondary, white }) => {
        if (primary) {
            return colors.PRIMARY;
        }
        if (secondary) {
            return colors.SECONDARY;
        }
        if (white) {
            return colors.WHITE;
        }
        return colors.PRIMARY;
    }};
`;

const renderIcon = ({ icon, ...other }) => {
    switch (icon) {
        case 'menu':
            return <MenuIcon {...other} />;
        default:
            return null;
    }
};

const Icon = ({ icon, width, height, ...props }) => {
    return (
        <ColorIcon {...props}>{renderIcon({ icon, width, height })}</ColorIcon>
    );
};

Icon.defaultProps = {
    className: '',
    height: 30,
    width: 30,
    primary: false,
    secondary: false,
    white: false
};

Icon.propTypes = {
    className: PropTypes.string,
    icon: PropTypes.string.isRequired,
    height: PropTypes.number,
    width: PropTypes.number,
    primary: PropTypes.bool,
    secondary: PropTypes.bool,
    white: PropTypes.bool
};

export default Icon;
