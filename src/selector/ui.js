export const getUiSettings = store => store.uiSettings;

export const getIsMenuOpen = store =>
    getUiSettings(store) ? getUiSettings(store).isMenuOpen : false;
