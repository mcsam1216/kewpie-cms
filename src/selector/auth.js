export const getAuth = store => store.auth;

export const getIsLogin = store => getAuth(store);
