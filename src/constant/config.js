export default {
    SERVICE: {
        VERSION: 'V1',
        BASE: process.env.API_URL
    },
    TOKEN_KEY: 'access_token'
};
