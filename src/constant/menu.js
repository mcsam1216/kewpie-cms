export default {
    PRODUCT: 'Product',
    PRODUCT_CATEGORY: 'Product Category',
    FOOD_SERVICE: 'Food Service',
    FOOD_SERVICE_CATEGORY: 'Food Service Category'
};
