export const colors = {
    PRIMARY: '#e4001b',
    SECONDARY: '#ff9900',
    SUCCESS: '#33cc33',
    WARNING: '#ffcc00',
    DANGER: '#ff3300',
    INFO: '#0099ff',
    LIGHT: '#d9d9d9',
    WHITE: '#ffffff',
    BLACK: '#000000',
    HOVER: '#dcdcdc'
};

export const measurements = {
    HEADER_HEIGHT: '60px',
    SIDEMENU_WIDTH: '200px',
    BUTTON_BORDER_RADIUS: '5px'
};

export const sizes = {
    mobileS: '320px',
    mobileM: '375px',
    mobileL: '425px',
    tablet: '768px',
    laptop: '1024px',
    laptopL: '1440px',
    desktop: '2560px'
};

export const devices = {
    mobileS: `(min-width: ${sizes.mobileS})`,
    mobileM: `(min-width: ${sizes.mobileM})`,
    mobileL: `(min-width: ${sizes.mobileL})`,
    tablet: `(min-width: ${sizes.tablet})`,
    laptop: `(min-width: ${sizes.laptop})`,
    laptopL: `(min-width: ${sizes.laptopL})`,
    desktop: `(min-width: ${sizes.desktop})`
};

export const transparency = {
    100: 'FF',
    95: 'F2',
    90: 'E6',
    85: 'D9',
    80: 'CC',
    75: 'BF',
    70: 'B3',
    65: 'A6',
    60: '99',
    55: '8C',
    50: '80',
    45: '73',
    40: '66',
    35: '59',
    30: '4D',
    25: '40',
    20: '33',
    15: '26',
    10: '1A',
    5: '0D',
    0: '00'
};

export const fonts = {
    FONT_FAMILY: "'IBM Plex Sans', sans-serif",
    FONT_SIZE_XS: '0.5em',
    FONT_SIZE_S: '0.8em',
    FONT_SIZE_M: '1.0em',
    FONT_SIZE_L: '1.4em',
    FONT_SIZE_XL: '1.8em'
};
