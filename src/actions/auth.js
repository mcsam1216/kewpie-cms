import {
    LOGOUT,
    LOGIN_SUCCESS,
    LOGIN_STARTED,
    LOGIN_FAILED
} from './action-types';
import getToken from '../services/auth';
import config from '../constant/config';

const loginStarted = () => ({
    type: LOGIN_STARTED,
    payload: {
        isLoading: true
    }
});

const loginSuccess = () => ({
    type: LOGIN_SUCCESS
});

const loginFailed = error => ({
    type: LOGIN_FAILED,
    payload: {
        error
    }
});

export const login = credential => {
    return dispatch => {
        dispatch(loginStarted());
        getToken(credential)
            .then(({ data: { token } }) => {
                sessionStorage.setItem(config.TOKEN_KEY, token);
                dispatch(loginSuccess());
            })
            .catch(({ response: { data: { message } } }) =>
                dispatch(loginFailed(message))
            );
    };
};

export const logout = () => {
    return dispatch => {
        sessionStorage.removeItem(config.TOKEN_KEY);
        dispatch({
            type: LOGOUT
        });
    };
};
