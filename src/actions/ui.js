import { TOGGLE_MENU } from './action-types';

// eslint-disable-next-line import/prefer-default-export
export const toggleMenu = () => ({ type: TOGGLE_MENU });
