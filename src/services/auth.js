import axios from 'axios';
import config from '../constant/config';

const baseURL = config.SERVICE.BASE;
const protectedURL = `${baseURL}/${config.SERVICE.VERSION}`;
const getToken = async credential =>
    axios.post(`${baseURL}/auth/token`, credential);

export default getToken;
