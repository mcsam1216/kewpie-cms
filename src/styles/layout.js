import styled from 'styled-components';
import { measurements, colors } from '../constant';

export const Header = styled.div`
    width: 100%;
    position: sticky;
    display: flex;
    justify-content: flex-start;
    box-shadow: 5px 2px 5px ${colors.HOVER};
    & > * {
        align-self: center;
    }
    padding: 0 25px;
    height: ${measurements.HEADER_HEIGHT};
    background: ${colors.PRIMARY};
`;

export const Body = styled.div`
    display: flex;
`;
