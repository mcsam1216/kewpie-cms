import styled from 'styled-components';
import { measurements, colors, transparency, fonts } from '../constant';

export const Container = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 100vh;
    font-family: 'Open Sans', sans-serif;
`;

export const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-evenly;
    width: 500px;
    height: 40%;
`;

export const Title = styled.h1`
    color: black;
    font-size: 2.5rem;
    font-weight: 700;
`;

export const Paragraph = styled.p`
    color: black;
    font-size: 1rem;
`;

export const Link = styled.a`
    color: #00a8e8;
`;

const getColor = color => {
    switch (color) {
        case 'primary':
            return colors.PRIMARY;
        case 'secondary':
            return colors.SECONDARY;
        case 'success':
            return colors.SUCCESS;
        case 'warning':
            return colors.WARNING;
        case 'danger':
            return colors.DANGER;
        case 'info':
            return colors.INFO;
        default:
            return colors.PRIMARY;
    }
};

export const Button = styled.button`
    border-radius: ${measurements.BUTTON_BORDER_RADIUS};
    padding: 0 16px;
    background: ${props => getColor(props.color)};
    color: ${colors.WHITE};
    font-size: ${fonts.FONT_SIZE_M};
    font-weight: bold;
    display: flex;
    height: 2.2rem;
    align-items: center;
    justify-content: space-around;
    cursor: pointer;
    &:hover {
        background: ${props => `${getColor(props.color)}${transparency[70]}`};
    }
    & > svg {
        margin-right: 5px;
    }
`;

export const OutlineButton = styled(Button)`
    background: transparent;
    border: solid 1px ${props => getColor(props.color)};
    color: ${props => getColor(props.color)};
`;
