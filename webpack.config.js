require('@babel/register');
const webpackMerge = require('webpack-merge');

const common = require('./config/webpack/webpack.common');

// eslint-disable-next-line import/no-dynamic-require
const envConfig = require(`./config/webpack/webpack.${process.env.NODE_ENV === 'production' ? 'prod' : 'dev'}`);
module.exports = webpackMerge(common, envConfig);
