import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import SpriteLoaderPlugin from 'svg-sprite-loader/plugin';
import Dotenv from 'dotenv-webpack';

import paths from './paths';
import rules from './rules';

module.exports = {
    entry: paths.entryPath,
    module: {
        rules
    },
    plugins: [
        new webpack.ProgressPlugin(),
        new HtmlWebpackPlugin({
            template: paths.templatePath,
            minify: {
                collapseInlineTagWhitespace: true,
                collapseWhitespace: true,
                preserveLineBreaks: true,
                minifyURLs: true,
                removeComments: true,
                removeAttributeQuotes: true
            }
        }),
        new CopyWebpackPlugin([
            {
                from: paths.imagesPath,
                to: paths.imagesFolder
            },
            {
                from: paths.iconsPath,
                to: paths.iconsFolder,
                ignore: ['**/static/**/*']
            }
        ]),
        new Dotenv({ safe: true }),
        new SpriteLoaderPlugin({ plainSprite: true })
    ]
};
