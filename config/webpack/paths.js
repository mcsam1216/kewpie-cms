import path from 'path';

module.exports = {
    root: path.resolve(__dirname, '..', '..'),
    outputPath: path.resolve(__dirname, '..', '..', 'dist'),
    entryPath: path.resolve(__dirname, '..', '..', 'src', 'index.js'),
    templatePath: path.resolve(__dirname, '..', '..', 'src', 'index.html'),
    imagesPath: path.resolve(__dirname, '..', '..', 'src', 'assets', 'images'),
    iconsPath: path.resolve(__dirname, '..', '..', 'src', 'assets', 'icons'),
    staticIconsPath: path.resolve(
        __dirname,
        '..',
        '..',
        'src',
        'assets',
        'static',
        'icons'
    ),
    constantPath: path.resolve(__dirname, '..', '..', 'src', 'constant'),
    servicePath: path.resolve(__dirname, '..', '..', 'src', 'services'),
    staticIconPath: path.resolve(
        __dirname,
        '..',
        '..',
        'src',
        'assets',
        'icons',
        'static'
    ),
    dynamicIconPath: path.resolve(
        __dirname,
        '..',
        '..',
        'src',
        'assets',
        'icons',
        'dynamic'
    ),
    imagesFolder: 'assets/images/',
    iconsFolder: 'assets/icons/',
    staticIconFolder: 'assets/icons/static/'
};
