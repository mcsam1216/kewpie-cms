import path from './paths';

module.exports = [
    {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
            loader: 'babel-loader',
            options: {
                presets: ['@babel/preset-env', '@babel/preset-react'],
                plugins: [
                    ['@babel/plugin-proposal-decorators', { legacy: true }],
                    [
                        '@babel/plugin-proposal-class-properties',
                        { loose: true }
                    ],
                    '@babel/plugin-proposal-export-default-from',
                    '@babel/plugin-proposal-object-rest-spread',
                    '@babel/plugin-proposal-export-namespace-from',
                    '@babel/plugin-syntax-dynamic-import'
                ]
            }
        }
    },
    {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        exclude: /node_modules/,
        loader: 'file-loader'
    },
    {
        test: /\.(woff|woff2)$/,
        exclude: /node_modules/,
        loader: 'url-loader?prefix=font/&limit=5000'
    },
    {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        exclude: /node_modules/,
        loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
    },
    {
        test: /\.(jpe?g|png|gif)$/i,
        use: ['url-loader?limit=10000', 'img-loader']
    },
    {
        test: /\.svg$/,
        include: [path.staticIconPath],
        use: [
            {
                loader: 'svg-sprite-loader',
                options: {
                    extract: true,
                    publicPath: path.staticIconFolder
                }
            },
            'svg-transform-loader',
            'svgo-loader'
        ]
    },
    {
        test: /\.svg$/,
        include: [path.dynamicIconPath],
        use: [
            {
                loader: 'react-svg-loader',
                options: {
                    svgo: {
                        plugins: [
                            { cleanupIDs: false },
                            { collapseGroups: false },
                            { removeUselessStrokeAndFill: false },
                            { removeUnknownsAndDefaults: false }
                        ],
                        floatPrecision: 2
                    }
                }
            }
        ]
    },
    {
        test: /\.css$/,
        use: [
            {
                loader: 'style-loader'
            },
            {
                loader: 'css-loader'
            }
        ]
    },
    {
        test: /\.s(a|c)ss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
    }
];
